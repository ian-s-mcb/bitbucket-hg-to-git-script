root_abs_path='/home/ian/dev/bitbucket_hg_to_git'
input_dir_name='step-2-output'
input_abs_path="${root_abs_path}/${input_dir_name}"
repo_names_file='repo_names'
owner='ian_s_mcb'

echo '[BB-HG-TO-GIT] - starting step-3 [push]'

if [ ! -f ${repo_names_file} ]; then
  echo 'Error: missing repo_names file'
  exit 1
fi

while read repo; do
  echo "[BB-HG-TO-GIT] - current repo: '${repo}'"

  repo_dir="${input_abs_path}/${repo}"
  if [ ! -d ${repo_dir} ]; then
    echo 'Error: cannot push repo. Repo dir is missing.'
    exit 1
  fi
  cd ${repo_dir}

  git remote add origin git@bitbucket.org:${owner}/${repo}.git
  git push origin master
done < ${repo_names_file}

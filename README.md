# bitbucket-hg-to-git-script

Script for migrating Mercurial (HG) repos on Bitbucket to  GIT repos
(also on Bitbucket)

### Motivation

Bitbucket is [ending Mercurial support][blog], therefore HG repos need
to replaced with GIT ones. Nobody has time to manually clone all their
old HG repos, run the repo converter tool on them, and push up the GIT
ones. This wouldn't be much of a problem, but BitBucket has [a
ridiculously incoherent documentation][docs] for their REST API.

### What this automates

- Cloning of HG repos
- Creating of empty GIT repos
- Transfering of history from HG repo to GIT repo
- Pushing GIT repos to Bitbucket

### What this doesn't automate

- Making a list of HG repos to be exported/tranfered
- Deleting of old HG repos
- Creating of new GIT repos (on Bitbucket.org) because you can't push to
  a remote that doesn't exist

## Getting started

These steps assume you have mercurial installed (via Pip) and Python 2.x
available (the [fast-export tool][tool] I borrowed doesn't support
Python 3).

```bash
# Clone this repo
cd bitbucket-hg-to-git-script

# Clone HG export tool
git clone https://github.com/frej/fast-export.git

# Create repo_names file
cp repo_name.example repo_names
# Edit to taste

# Load SSH key using ssh-agent
eval $(ssh-agent)
ssh-add ~/.ssh/path/to/private/ssh/key

# Run script
./run.sh <bitbucket_username>
```

[blog]: https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket
[docs]: https://developer.atlassian.com/server/bitbucket/reference/rest-api/
[tool]: https://github.com/frej/fast-export

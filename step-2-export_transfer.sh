root_abs_path=$PWD
input_dir_name='step-1-output'
input_abs_path="${root_abs_path}/${input_dir_name}"
output_dir_name='step-2-output'
output_abs_path="${root_abs_path}/${output_dir_name}"
repo_names_file='repo_names'
tool="${root_abs_path}/fast-export/hg-fast-export.sh"

echo '[BB-HG-TO-GIT] - starting step-2 [export_transfer]'

if [ ! -f $tool ]; then
  echo 'Error: missing fast-export tool. Tool must be cloned into ' \
  'this repo.'
  exit 1
elif [ ! -f ${repo_names_file} ]; then
  echo 'Error: missing repo_names file'
  exit 1
fi

mkdir -p "${output_abs_path}"
while read repo; do
  echo "[BB-HG-TO-GIT] - current repo: '${repo}'"
  cd "${output_abs_path}"
  echo '[BB-HG-TO-GIT] - initialize empty Git repo'
  git init "$repo"
  cd "${repo}"
  input_repo_abs="${input_abs_path}/$repo"
  echo '[BB-HG-TO-GIT] - export and transfer repo history'
  ${tool} -r ${input_repo_abs}
  git checkout HEAD
done < ${repo_names_file}

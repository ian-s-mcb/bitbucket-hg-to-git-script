if [ -lt $# 1 ]; then
  echo 'Error: missing argument for repo owner, e.g. ian_s_mcb'
  exit 1
fi

root_abs_path=$PWD
output_dir_name='step-1-output'
output_abs_path="${root_abs_path}/${output_dir_name}"
owner=$1
repo_names_file='repo_names'

echo '[BB-HG-TO-GIT] - starting step-1 [clone]'

if [ ! -f ${repo_names_file} ]; then
  echo 'Error: missing repo_names file'
  exit 1
fi

mkdir -p "${output_abs_path}"
while read repo; do
  echo "[BB-HG-TO-GIT] - current repo: '${repo}'"
  cd "${output_abs_path}"
  hg clone ssh://hg@bitbucket.org/"${owner}/$repo"
done < repo_names

if [ $# -lt 1 ]; then
  echo 'Error: missing argument for repo owner, e.g. ian_s_mcb'
  exit 1
fi

owner="$1"

./step-1-clone.sh ${owner} && \
./bash step-2-export_transfer.sh

# Commented out last step FOR A REASON
# - User must delete their HG repos prior to pushing their GIT ones
# - BitBucket doesn't allow HG and GIT repos to share the same name
# Visit https://bitbucket.org/
# - Delete all HG repos
# - Create all (empty) GIT repos with the same names
# Run step-3.sh - remember to provide owner argument

#./step-3-push.sh ${owner}
